OpenDTG(Open Digital Twin Generator)
=====================================


Introduction
------------

[OpenDT (Open Digital Twin)] "Open Digital Twin" is platform for creating and managing digital twin that replicate the real world in real time.

We mainly focus on follows,

1. Automatic generation of cadastral 3D data from serveral data sources.(GML, OSM, and so on.)
2. Spatial map generation mothod for camera localization especially targeted to the Augmented Reality community. 
3. Monitoring and updating instances in digital twin on real time.

<!--
[![GitHub license]]
-->

- [Build](#build)
- [Continuous integration](#integration)
- [Authors](#authors)
- [Contact](#contact)
- [Citations](#citations)
- [Acknowledgements](#acknowledgements)

## Build

Please follow this [build tutorial ]

## Continuous integration


## Authors

See [Authors]("available soon") text file

## Documentation

See [documentation]("available soon")

## Contact

http://arrc.kaist.ac.kr/about


## Citations


## Acknowledgements



