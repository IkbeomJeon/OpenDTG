﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;
using ARRC.DigitalTwinGenerator;
using System.IO;
using UnityEditor;
using ARRC.Framework;

namespace CityGML2GO.GmlHandlers
{
    public class BuildingHandler
    {
        public static void HandleBuilding(XmlReader reader, CityGml2GO option, Transform parent, CityGMLGenerator cityGml2Go)
        {
            var buildingName = "no id";
            var layerName = "Building";

            while (reader.MoveToNextAttribute())
            {
                if (reader.LocalName == "id")
                {
                    buildingName = reader.Value;
                }
            }

            //Create folder on the building.
            string resultFolder = Path.Combine(ARRCPaths.ResultFolder_Building(option.title), buildingName);

            if (!Directory.Exists(resultFolder)) Directory.CreateDirectory(resultFolder);


            var buildingGo = new GameObject(buildingName);
            //var buildingProperties = buildingGo.AddComponent<Scripts.BuildingProperties>();
            //var semanticType = buildingGo.AddComponent<SemanticType>();
            buildingGo.transform.parent = parent;
            buildingGo.layer = LayerMask.NameToLayer(layerName);

            //Building node.
            while (reader.Read())
            {
                //if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "X3DMaterial")
                //{
                //    MaterialHandler.HandleMaterial(reader, cityGml2Go);
                //}

                //if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "ParameterizedTexture")
                //{
                //    TextureHandler.HandleTexture(reader, cityGml2Go);
                //}


                if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "Polygon")
                {
                    try
                    {
                        //Mesh polygonMesh = PolygonHandler.PolyToMesh(reader, option, semanticType);
                        Mesh polygonMesh = PolygonHandler.PolyToMesh(reader, option);

                        GameObject go_polygon = new GameObject(polygonMesh.name);
                        go_polygon.AddComponent<MeshRenderer>().sharedMaterial = option.DefaultMaterial;
                        go_polygon.transform.SetParent(buildingGo.transform);
                        go_polygon.layer = LayerMask.NameToLayer(layerName);

                        if (option.useSharedMemory)
                        {
                            go_polygon.AddComponent<MeshFilter>().sharedMesh = polygonMesh;
                            
                        }
                        else
                        {
                            string fullFilePath = Path.Combine(resultFolder, polygonMesh.name + ".asset");
                            string assetFilePath = ARRCPaths.GetAssetPath(fullFilePath);

                            //mesh 저장.
                            AssetDatabase.CreateAsset(polygonMesh, assetFilePath);
                            AssetDatabase.SaveAssets();

                            //저장된 메쉬로 생성.
                            go_polygon.AddComponent<MeshFilter>().mesh = (Mesh)AssetDatabase.LoadAssetAtPath(assetFilePath, typeof(Mesh));
                        }
                    }
                    catch(IDNotFoundException)
                    {
                        continue;
                    }
                    catch (Exception ex)
                    {
                        //Debug.LogWarning(ex.Message);
                        Debug.LogException(ex);
                        continue;
                    }
                    
                }

                //if (option.ShowCurves)
                //{
                //    if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "MultiCurve")
                //    {
                //        MultiCurveHandler.HandleMultiCurve(reader, buildingGo, semanticType, option);
                //    }
                //}

                //if (option.Semantics)
                //{
                //    if (reader.NodeType == XmlNodeType.Element && option.SemanticSurfaces.Any(x => x == reader.LocalName))
                //    {
                //        semanticType.Name = reader.LocalName;
                //        reader.MoveToFirstAttribute();
                //        if (reader.LocalName == "id")
                //        {
                //            semanticType.Id = reader.Value;
                //        }
                //        else
                //        {
                //            while (reader.MoveToNextAttribute())
                //            {
                //                semanticType.Id = reader.Value;
                //            }
                //        }
                //    }
                //}

                //BuildingPropertiesHandler.HandleBuildingProperties(reader, buildingProperties);

                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "Building")
                {
                    break;
                }
            }

            //UnityEngine.Object.DestroyImmediate(semanticType);
        }
       
    }
}
