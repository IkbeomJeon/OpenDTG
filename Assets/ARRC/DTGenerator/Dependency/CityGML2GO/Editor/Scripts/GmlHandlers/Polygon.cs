﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;
using Proj4Net;
using ARRC.DigitalTwinGenerator;
using System.IO;
using UnityEditor;

namespace CityGML2GO.GmlHandlers
{
    public class PolygonHandler
    {

        public static Mesh PolyToMesh(XmlReader reader, CityGml2GO option, SemanticType semanticType = null)
        {
            var extPositions = new List<Vector3>();
            var intPositions = new List<List<Vector3>>();
            
            string polyName = null; // Guid.NewGuid().ToString(); 

            while (reader.MoveToNextAttribute())
            {
                if (reader.LocalName == "id")
                {
                    polyName = reader.Value;
                }
            }

            while (reader.Read())
            {
                while (reader.MoveToNextAttribute())
                {
                    if (reader.LocalName == "id")
                    {
                        polyName = reader.Value;
                    }
                }

            

                if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "exterior")
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "posList")
                        {
                            var range = PositionHandler.GetPosList(reader, option.coordinateInfo);
                            if (range != null)
                            {
                                extPositions.AddRange(range);
                            }

                        }

                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "pos")
                        {
                            extPositions.Add(PositionHandler.GetPos(reader, option.coordinateInfo));
                        }

                        if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "exterior")
                        {
                            break;
                        }
                    }
                }

                if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "interior")
                {
                    var curInt = new List<Vector3>();
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "posList")
                        {
                            var range = PositionHandler.GetPosList(reader, option.coordinateInfo);
                            if (range != null)
                            {
                                curInt.AddRange(range);
                            }
                        }

                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "pos")
                        {
                            curInt.Add(PositionHandler.GetPos(reader, option.coordinateInfo));
                        }
                        if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "interior")
                        {
                            break;
                        }
                    }
                    intPositions.Add(curInt);
                }

                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "Polygon")
                {
                    break;
                }
            }
            if (string.IsNullOrEmpty(polyName))
            {
                IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                int lineNumber = xmlInfo.LineNumber;
                throw new IDNotFoundException(string.Format("The polygon ID is not found : line {0}, It is ignored.", lineNumber));
                
            }

            if (!IsPolyValid(extPositions))
            {
                IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                int lineNumber = xmlInfo.LineNumber;
                throw new Exception(string.Format("Invalid polygons exist on line number {0} in gml", lineNumber));
            }


            foreach (var intRing in intPositions)
            {
                if (!IsPolyValid(intRing))
                {
                    IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                    int lineNumber = xmlInfo.LineNumber;
                    throw new Exception(string.Format("Invalid polygons exist on line number {0} in gml", lineNumber));
                }
            }

            var polygon = new Poly2Mesh.Polygon();

            polygon.oriExt = extPositions.ConvertAll(ext => new Vector3(ext.x, ext.y, ext.z));
            extPositions.Reverse();
            intPositions.Reverse();

            polygon.outside = extPositions;
            polygon.holes = intPositions;
            polygon.name = polyName;

            Mesh mesh = Poly2Mesh.CreateMesh(polygon);
            mesh.name = polyName;

            return mesh;
        }
        public static GameObject PolyToGameObject(XmlReader reader, string id, CityGml2GO option, CityGMLGenerator cityGml2GO, SemanticType semanticType = null)
        {
            var extPositions = new List<Vector3>();
            var intPositions = new List<List<Vector3>>();
            var polyName = "";

            while (reader.MoveToNextAttribute())
            {
                if (reader.LocalName == "id")
                {
                    polyName = reader.Value;
                }
            }

            while (reader.Read())
            {
                if (((IXmlLineInfo)reader).LineNumber >= 14459)
                {
                    //int idfg = 0;
                    //Debug.LogWarning("LineNumber >= 14459");

                }
                if (string.IsNullOrEmpty(polyName))
                {
                    while (reader.MoveToNextAttribute())
                    {
                        if (reader.LocalName == "id")
                        {
                            polyName = reader.Value;
                        }
                    }
                }

                if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "exterior")
                {

                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "posList")
                        {
                            var range = PositionHandler.GetPosList(reader, option.coordinateInfo);
                            if (range != null)
                            {
                                extPositions.AddRange(range);
                            }

                        }

                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "pos")
                        {
                            extPositions.Add(PositionHandler.GetPos(reader, option.coordinateInfo));
                        }

                        if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "exterior")
                        {
                            break;
                        }
                    }
                }

                if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "interior")
                {
                    //Debug.Log("interior");

                    var curInt = new List<Vector3>();
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "posList")
                        {
                            var range = PositionHandler.GetPosList(reader, option.coordinateInfo);
                            if (range != null)
                            {
                                curInt.AddRange(range);
                            }
                        }

                        if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "pos")
                        {
                            curInt.Add(PositionHandler.GetPos(reader, option.coordinateInfo));
                        }
                        if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "interior")
                        {
                            break;
                        }
                    }
                    //Debug.Log("interior call");
                    intPositions.Add(curInt);
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.LocalName == "Polygon")
                {
                    break;
                }
            }

            //var lastPoint = extPositions.Last();
            //extPositions = extPositions.Distinct().ToList();
            //extPositions.Add(lastPoint);

//            for (var index = 0; index < intPositions.Count; index++)
//            {
//                lastPoint = intPositions[index].Last();
//                //intPositions[index] = intPositions[index].Distinct().ToList();
//                intPositions[index].Add(lastPoint);
//            }


            if (!PolygonHandler.IsPolyValid(extPositions))
            {
                IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                int lineNumber = xmlInfo.LineNumber;
                Debug.Log(lineNumber);
                return null;
            }


            foreach (var intRing in intPositions)
            {
                if (!IsPolyValid(intRing))
                {
                    IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                    int lineNumber = xmlInfo.LineNumber;
                    Debug.Log(lineNumber);
                    return null;
                }
            }


            //영역 검사

            //Polygon들의 센터를 중심으로 좌표계 변환

            //Vector3 center_ext;
            //ConvertCoordinate(extPositions, intPositions, out center_ext);

            //ConvertUnityCoordinate(option.crsTarget, extPositions);
            //foreach (var intRing in intPositions)
            //    ConvertUnityCoordinate(option.crsTarget, intRing);

            var polygon = new Poly2Mesh.Polygon();

			polygon.oriExt = extPositions.ConvertAll(ext => new Vector3(ext.x,ext.y, ext.z));
            extPositions.Reverse();
            intPositions.Reverse();

            polygon.outside = extPositions;
            polygon.holes = intPositions;

            polygon.name = polyName;
            cityGml2GO.oriPoly.Add(polygon);

            GameObject go = null;

            try
            {
                go = Poly2Mesh.CreateGameObject(polygon, string.IsNullOrEmpty(polyName) ? id : polyName);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }

            if (option.GenerateColliders)
            {
                //go.AddComponent<MeshCollider>();
            }

            if (go != null)
            {
                if (option.Semantics && semanticType != null)
                {
                    SemanticsHandler.HandleSemantics(go, semanticType, option);
                }
                else if (option.DefaultMaterial!= null)
                {
                    var mr = go.GetComponent<MeshRenderer>();
                    if (mr != null)
                    {
                        mr.sharedMaterial = option.DefaultMaterial;
                    }
                }

            }


            cityGml2GO.Polygons.Add(go);

            return go;
        }

        public static bool IsPolyValid(List<Vector3> poly)
        {
            if (poly.First() != poly.Last())
            {
                Debug.LogWarning("First != Last");
                return false;
            }

            if (poly.Count < 4)
            {
                Debug.LogWarning("Count < 4");
                return false;
            }

            return true;
        }


        static bool CheckInner(double minX, double minY, double maxX, double maxY, Vector3 pos)
        {
            if (pos.x < minX || pos.x > maxX || pos.z < minY || pos.y > maxY)
                return false;

            return true;
        }

        //static void ConvertUnityCoordinate(string crsName, List<Vector3> posList)
        //{
        //    const string WGS84_PARAM = "+title=long/lat:WGS84 +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees";

        //    CoordinateReferenceSystemFactory crsFactory = new CoordinateReferenceSystemFactory();

        //    CoordinateReferenceSystem source = crsFactory.CreateFromName(crsName);
        //    CoordinateReferenceSystem target = crsFactory.CreateFromParameters("WGS84", WGS84_PARAM);

        //    for (int i = 0; i < posList.Count; i++)
        //    {
        //        ProjCoordinate prjSrc = new ProjCoordinate(posList[i].x, posList[i].z);
        //        ProjCoordinate prjTgt = new ProjCoordinate();

        //        BasicCoordinateTransform t1 = new BasicCoordinateTransform(source, target);
        //        t1.Transform(prjSrc, prjTgt);

        //        Vector3 worldPos = Vector3.zero;

        //        //convert unity 
        //        //posList[i] = CoordinateConverter.LatLonToWorldWithElevation(CoordinateInfo.Instance, prjTgt.Y, prjTgt.X);
        //        //posList[i] = CoordinateConverter.LatLonToWorldWithElevation(CoordinateInfo.Instance, prjTgt.Y, prjTgt.X);
        //        Vector2 posXZ = CoordinateConverter.LatLonToWorldXZ(CoordinateInfo.Instance, prjTgt.Y, prjTgt.X);
        //        posList[i] = new Vector3(posXZ.x, posList[i].y, posXZ.y);
        //    }
        //}

        static void ConvertCoordinate(List<Vector3> extList, List<List<Vector3>> intList, out Vector3 center)
        {
            double cx = 0, cy = 0, cz = 0;

            int count = 0;

            //센터 구하기
            foreach (Vector3 pos in extList)
            {
                count++;
                cx += pos.x;
                cy += pos.y;
                cz += pos.z;
            }

            foreach (List<Vector3> ringList in intList)
            {
                foreach (Vector3 pos in ringList)
                {
                    count++;
                    cx += pos.x;
                    cy += pos.y;
                    cz += pos.z;
                }
            }
            center = new Vector3((float)(cx / count), (float)(cy / count), (float)(cz / count));


            //좌표 이동
            for (int i = 0; i < extList.Count; i++)
            {
                Vector3 ori = extList[i];
                extList[i] = new Vector3(ori.x - center.x, ori.y - center.y, ori.z - center.z);
            }
            foreach (List<Vector3> ringList in intList)
            {
                for (int i = 0; i < ringList.Count; i++)
                {
                    Vector3 ori = ringList[i];
                    ringList[i] = new Vector3(ori.x - center.x, ori.y - center.y, ori.z - center.z);
                }
            }
        }
      
    }
}
