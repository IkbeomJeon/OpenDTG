﻿using System.Collections.Generic;
using UnityEngine;


namespace CityGML2GO
{
    public class TextureInformation
    {
        public string Url;
        public List<TextureTarget> Targets = new List<TextureTarget>();
    }

    public class TextureTarget
    {
        public string Id;
        public List<Vector2> Coords = new List<Vector2>();
    }
}