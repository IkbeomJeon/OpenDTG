﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CityGMLModel
{
    public string version { get; private set; }
    public string epsg { get; private set; }
    public double[] lowerCorner { get; private set; }
    public double[] upperCorner { get; private set; }
    public List<GMLBuilding> buildings { get; private set; }

    private CityGMLModel(Builder builder)
    {
        version = builder.version;
        epsg = builder.epsg;
        lowerCorner = builder.lowerCorner;
        upperCorner = builder.upperCorner;
        buildings = builder.buildings;
    }

    public class Builder
    {
        public string version { get; private set; }
        public string epsg { get; private set; }
        public double[] lowerCorner { get; private set; }
        public double[] upperCorner { get; private set; }
        public List<GMLBuilding> buildings { get; private set; }

        public Builder SetVersion(string val)
        {
            version = val;
            return this;
        }
        public Builder SetEPSG(string val)
        {
            epsg = val;
            return this;
        }
        public Builder SetLowerCorner(double[] val)
        {
            lowerCorner = val;
            return this;
        }
        public Builder SetUpperCorner(double[] val)
        {
            upperCorner = val;
            return this;
        }
        public Builder SetBuildings(List<GMLBuilding> val)
        {
            buildings = val;
            return this;
        }
        public CityGMLModel Build()
        {
            return new CityGMLModel(this);
        }

    }

}

public class GMLPolygon
{
    public string id { get; private set; }
    public List<double> exterior { get; private set; }
    public List<double> interior { get; private set; }
    private GMLPolygon(Builder builder)
    {
        id = builder.id;
        exterior = builder.exterior;
        interior = builder.interior;
    }


    public class Builder
    {
        public string id { get; private set; }
        public List<double> exterior { get; private set; }
        public List<double> interior { get; private set; }


        public Builder SetID(string val)
        {
            id = val;
            return this;
        }

        public Builder SetExterior(List<double> val)
        {
            exterior = val;
            return this;
        }

        public Builder SetInterior(List<double> val)
        {
            interior = val;
            return this;
        }
        public GMLPolygon Build()
        {
            return new GMLPolygon(this);
        }
    }

}



public class GMLBuilding
{
    public string id { get; private set; }
    public string epsg { get; private set; }
    public double[] lowerCorner { get; private set; }
    public double[] upperCorner { get; private set; }
    public List<GMLPolygon> polygons { get; private set; }

    private GMLBuilding(Builder builder)
    {
        //id = builder.
        id = builder.id;
        epsg = builder.epsg;
        lowerCorner = builder.lowerCorner;
        polygons = builder.polygons;

    }
    public class Builder
    {
        public string id { get; private set; }
        public string epsg { get; private set; }
        public double[] lowerCorner { get; private set; }
        public double[] upperCorner { get; private set; }
        public List<GMLPolygon> polygons { get; private set; }

        public Builder SetID(string val)
        {
            id = val;
            return this;
        }

        public Builder SetEPSG(string val)
        {
            epsg = val;
            return this;
        }

        public Builder SetLowerCorner(double[] val)
        {
            lowerCorner = val;
            return this;
        }
        public Builder SetUpperCorner(double[] val)
        {
            upperCorner = val;
            return this;
        }
        public Builder SetPolygons(List<GMLPolygon> val)
        {
            polygons = val;
            return this;
        }

        public GMLBuilding Build()
        {
            return new GMLBuilding(this);
        }


    }
}
