﻿using AlexisGea;
using UnityEngine;

	public class SphereGeneratorStatic
	{
		static public Mesh GenerateMesh(SphereType sphereType, float radius, int resolution, bool smooth = true, bool remapVertices = true)
		{
			MeshData sphereMeshData;
			if (sphereType == SphereType.uvsphere)
				sphereMeshData = UvSphereBuilder.Generate(radius, resolution);
			else
			{
				IPlatonicSolid baseSolid = GetBaseSolid(sphereType);
				sphereMeshData = SphereBuilder.Build(baseSolid, radius, resolution, smooth, remapVertices);
			}

			Mesh mesh = new Mesh();
			mesh.Clear();
			mesh.name = sphereType.ToString();
			mesh.vertices = sphereMeshData.Vertices;
			mesh.triangles = sphereMeshData.Triangles;
			mesh.uv = sphereMeshData.Uv;
			mesh.normals = sphereMeshData.Normals;
			mesh.tangents = sphereMeshData.Tangents;

			return mesh;
		}
	private static IPlatonicSolid GetBaseSolid(SphereType type)
	{
		switch (type)
		{
			case SphereType.tetrasphere:
				return new Tetrahedron();

			case SphereType.cubesphere:
				return new Cube();

			case SphereType.octasphere:
				return new Octahedron();

			case SphereType.dodecasphere:
				return new Dodecahedron();

			case SphereType.icosphere:
				return new Icosahedron();

			case SphereType.uvsphere:
				return new UvPolyhedron();

			default:
				return new Icosahedron();
		}
	
	}

}
