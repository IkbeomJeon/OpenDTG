﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif   

namespace ARRC.DigitalTwinGenerator
{
#if UNITY_EDITOR
    [ExecuteInEditMode]
#endif

    [RequireComponent(typeof(StreetViewItemMono))]
    public class StreetViewControllerMono : MonoBehaviour
    {
        const float size_gizmo = 5f;

        bool showBuilding = true;
        bool bStart;
        float px, py;

        public GameObject panoCam;

        [SerializeField, HideInInspector]
        public StreetViewItem streetviewInfo;

        [HideInInspector]
        //public bool isSelected;
        public int selState = 0; //0 ; unsel, 1 ; only, 2; additionally

        void Awake()
        {
            panoCam = transform.Find("Pano Camera").gameObject;
            if (panoCam == null)
                Debug.LogError("This gameobject must include \"Pano Camera\" as child object.");
        }

        //선택, zoom level에 따른 Gizmo 색상 결정.
        void OnDrawGizmos()
        {
            streetviewInfo = GetComponent<StreetViewItemMono>().streetviewInfo;
            if (streetviewInfo == null)
                Debug.LogError("The gameobject must include \"StreetViewItemMono\" as component first.");

            float alpha = 0.2f * streetviewInfo.loadedZoom;

            switch(selState)
            {
                case 0:
                    if(alpha < 0)
                        Gizmos.color = new Color(0f, 0f, 0f, 0.2f);
                    else
                        Gizmos.color = new Color(0f, 1f, 0f, alpha);

                    break;

                case 1:
                    Gizmos.color = new Color(1f, 0f, 0f, 0.8f);
                    break;
                case 2:
                    Gizmos.color = new Color(1f, 1f, 0f, 0.8f);
                    break;
            }

            Gizmos.DrawSphere(transform.position, transform.lossyScale.x * size_gizmo);
            Gizmos.color = new Color(1f, 0f, 1f, 1f);
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 2);

        }

        public void OnUnselected()
        {
            selState = 0;
            GetComponent<MeshRenderer>().enabled = false;
            panoCam.SetActive(false);
        }
        public void OnSelectedOnly()
        {
            selState = 1;
            GetComponent<MeshRenderer>().enabled = true;
            panoCam.SetActive(true);
        }
        public void OnSelectedAdditionaly()
        {
            selState = 2;
            GetComponent<MeshRenderer>().enabled = false;
            panoCam.SetActive(false);
        }

        void OnGUI()
        {
            if (selState == 1)
            {
#if UNITY_EDITOR
                if (Event.current.type == EventType.Layout || Event.current.type == EventType.Repaint)
                {
                    EditorUtility.SetDirty(this); // this is important, if omitted, "Mouse down" will not be display
                }
#endif
                if (Event.current.type == EventType.MouseDown && Event.current.button == 0) //left mouse down
                {
                    Event e = Event.current;
                    px = e.mousePosition.x;
                    py = e.mousePosition.y;
                }
                else if (Event.current.type == EventType.MouseDown && Event.current.button == 1) //right mouse down
                {
                    if (showBuilding)
                    {
                        panoCam.GetComponent<Camera>().cullingMask = 1;
                    }
                    else
                    {
                        panoCam.GetComponent<Camera>().cullingMask |= 1 << LayerMask.NameToLayer("Building");
                    }
                    showBuilding = !showBuilding;
                }
                if (Event.current.type == EventType.MouseDrag && Event.current.button == 0)
                {
                    Event e = Event.current;

                    float cx = e.mousePosition.x;
                    float cy = e.mousePosition.y;

                    float angle_axisY = (px - cx);
                    float angle_axisX = (py - cy);

                    panoCam.transform.Rotate(new Vector3(0, angle_axisY, 0), Space.World);
                    panoCam.transform.Rotate(new Vector3(angle_axisX, 0, 0), Space.Self);

                    px = cx;
                    py = cy;

                }
            }

        }


    }
}

