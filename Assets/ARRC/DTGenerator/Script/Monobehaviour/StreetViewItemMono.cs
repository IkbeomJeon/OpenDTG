﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ARRC.Framework;
using ARRC.Coordinate;

namespace ARRC.DigitalTwinGenerator
{
    public class StreetViewItemMono : ARRCGeneratorMonoBase
    {
        [SerializeField]
        public CoordinateInfo coordinateInfo;

        [SerializeField, HideInInspector]
        public StreetViewItem streetviewInfo;

        [SerializeField, HideInInspector]
        public TextureType loadedTextureType;

         [SerializeField, HideInInspector]
        public string loadedTexturePath;
     
    }
}