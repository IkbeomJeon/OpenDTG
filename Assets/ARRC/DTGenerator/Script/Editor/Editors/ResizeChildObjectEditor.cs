﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(ResizeChildObjectMono))]

public class ResizeChildObjectMonoEditor : Editor
{
    void OnSceneGUI()
    {
        ResizeChildObjectMono script = ((ResizeChildObjectMono)target);
        script.ResizeImageObjects();
    }

}

