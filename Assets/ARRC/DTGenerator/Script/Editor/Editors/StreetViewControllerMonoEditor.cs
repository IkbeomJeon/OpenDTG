﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ARRC.DigitalTwinGenerator
{
    [CustomEditor(typeof(StreetViewControllerMono)), CanEditMultipleObjects]
    public class StreetViewControllerMonoEditor : Editor
    {    
        private void OnEnable()
        {
            for(int i=0; i<targets.Length; i++)
            {
                var mono = targets[i] as StreetViewControllerMono;

                if (i == 0)
                    mono.OnSelectedOnly();
                else
                    mono.OnSelectedAdditionaly();
            }
        }

        private void OnDisable()
        {
            foreach(StreetViewControllerMono mono in targets)
                mono.OnUnselected();
        }


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }

    }
}