﻿namespace ARRC.DigitalTwinGenerator
{
    //public enum Zoom_Google
    //{
    //    1(832x416), 2(1664x832), 3(3328 x 1664), 4(6656 x 3328), 5(13312 x 6656),
    //}

    public enum TerrainTextureProvider
    {
        VWorld,
    }

    public enum TerrainElevationProvider
    {
      VWorld,
    }
    public enum BuldingProvider
    {
        VWorld, CityGML,
    }
    public enum StreetViewProvider
    {
        Google,
    }

    public enum TextureType
    {
        Color, Depth, Label
    }
}