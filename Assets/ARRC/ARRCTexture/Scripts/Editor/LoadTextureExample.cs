﻿using UnityEngine;
using ARRC.ARRCTexture;

public class LoadTest : MonoBehaviour
{
    void Start()
    {
        //string filepath = @"E:\@SmartMedia_Git\kctm2unity\Assets\ARRC_Result\berlin\StreetView\Depth\PseudoDepth\xFpG1ZXbiE3n6Kc8sSHXgQ.exr";
        string filepath = ARRC.Framework.ARRCPaths.GetGrayScaleDepthPath("berlin", "xFpG1ZXbiE3n6Kc8sSHXgQ.exr");

        //1. UnityEditor의 AssetDataBase를 이용하는 방법. (texture가 assets 폴더 내에 있어야함.)
        string assetpath = ARRC.Framework.ARRCPaths.GetAssetPath(filepath);
        Texture2D tex_fromAssets = TextureIO_Editor.LoadTextureUsingAssetDataBase(assetpath, 1024);
        Color color_fromAssets = tex_fromAssets.GetPixel(0, 0);
        Debug.Log(color_fromAssets.r.ToString());

        //2. OpenCV를 이용한 방법.
        Texture2D tex = TextureIO.LoadTextureFloat(filepath);
        Color color = tex.GetPixel(0, 0);
        Debug.Log(color.r.ToString());
    }

}
