﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChildrenMeterial : MonoBehaviour
{
    public Material material;
    MeshRenderer[] meshRenderers;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetSharedMeterial(Material newMaterial)
    {
        meshRenderers = transform.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in meshRenderers)
        {
            Color original = renderer.material.color;
            renderer.sharedMaterial.color = new Color(0, 0, 0, 0.5f);
        }
            
    }
    public void ButtonClick()
    {
        enabled = !enabled;
        gameObject.SetActive(enabled);
    }
}
