﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// Creates a custom Label on the inspector for all the scripts named ScriptName
// Make sure you have a ScriptName script in your
// project, else this will not work.

[CustomEditor(typeof(ChangeChildrenMeterial))]
public class ChangeChildrenMeterialEditor : Editor
{
    ChangeChildrenMeterial script;

    private void OnEnable()
    {
       
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        script = (ChangeChildrenMeterial)target;

        if (GUILayout.Button("Set meterial"))
        {
            if (script.material != null)
                script.SetSharedMeterial(script.material);
        }


    }

}
