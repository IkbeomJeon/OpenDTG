﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PSO;
using System;
using System.Runtime.InteropServices;

public class TestPSO : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CONFIG config = new CONFIG()
        {
            DIM = 6,
            NUM_PARTICLE = 30, //warning ; particle 50이상에서 iteration 4837에서 objective_function을 호출하며 에러 발생.
            ITERATION = 10000,
            STRATEGY_SOCIAL = (int)STRATEGY_SOCIAL.STRATEGY_GLOBAL,
            STRATEGY_WEIGHT = (int)STRATEGE_WEIGHT.STRATEGY_W_LIN_DEC,
            COUNT_NO_IMPROVEMENT = 10,

            ERROR_THRESHOLD = 1e-8,

            HALT_THRESHOLD = Convert.ToByte(true),
            HALT_ITERATION = Convert.ToByte(true),
            HALT_NO_IMPROVEMENT = Convert.ToByte(false),

            INERTIA_BOUND_LOW = 0.3,
            INERTIA_BOUND_HIGH = 0.9,

            C1 = 1.496,
            C2 = 1.496,

            STD_INITIAL_POSE = 0.01 // 0.05 is standard.
        };

        RANGE[] arrBounds = new RANGE[config.DIM];
        double[] arrInitPoint = new double[config.DIM];

        for (int i = 0; i < config.DIM; i++)
        {
            arrBounds[i].lower = -9e3;
            arrBounds[i].upper = 9e3; //low - high

            arrInitPoint[i] = UnityEngine.Random.Range((float)arrBounds[i].lower, (float)arrBounds[i].upper);
        }
        

        ParticleSwarmOptimizationWarpper.Instance.InitSwarm(config, arrInitPoint, arrBounds, new Callback(pso_griewank));
        RESULT result = ParticleSwarmOptimizationWarpper.Instance.Optimzation(config);
        Debug.Log("Minimum Point Reached: " + result.gBest_Err);

        ParticleSwarmOptimizationWarpper.Instance.InitSwarm(config, arrInitPoint, arrBounds, new Callback_Type2(pso_griewank_for_all_particle_Swarm1D));
        result = ParticleSwarmOptimizationWarpper.Instance.Optimzation(config);
        Debug.Log("Minimum Point Reached: " + result.gBest_Err);

        //ParticleSwarmOptimizationWarpper.Instance.DestroySwarm();
    }

    static double pso_griewank(IntPtr unmanaged_particle, int dim)
    {
        double[] particle = new double[dim];

        Marshal.Copy(unmanaged_particle, particle, 0, particle.Length);

        double sum = 0, product = 1;
        for (int i = 0; i < dim; i++)
        {
            sum += Math.Pow(particle[i], 2);
            product *= Math.Cos(particle[i] / Math.Sqrt(i + 1));
        }
        return (1 + (sum / 4000) - product);
    }


    static double[][] MarshalJuggedFromC(IntPtr unmanagedPtr, int col, int row)
    {
        double[][] managedArray = new double[col][];

        int sizeofPtr = Marshal.SizeOf(typeof(IntPtr));

        for (int i = 0; i < col; i++)
        {
            managedArray[i] = new double[row];
            IntPtr v1 = Marshal.ReadIntPtr(unmanagedPtr, i * sizeofPtr);
            Marshal.Copy(v1, managedArray[i], 0, row);
            //Marshal.FreeCoTaskMem(v1);
            //Marshal.FreeHGlobal(v1);
            //for (int j = 0; j < row; j++)
            //{
            //    managedArray[i][j] = 1;
            //    //Console.Write(managedArray[i][j].ToString() + " ");
            //}
            ////Console.WriteLine();
        }
        //Marshal.FreeCoTaskMem(unmanagedPtr);
        //Marshal.FreeHGlobal(unmanagedPtr);
        return managedArray;
    }

    static IntPtr MarshalJuggedToC(double[] managedArray)
    {
        // Initialize unmanaged memory to hold the array.
        int size = Marshal.SizeOf(managedArray[0]) * managedArray.Length;
        IntPtr pnt = Marshal.AllocCoTaskMem(size);

        // Copy the array to unmanaged memory.
        Marshal.Copy(managedArray, 0, pnt, managedArray.Length);

        return pnt;
    }


    IntPtr pso_griewank_for_all_particle_Swarm1D(IntPtr unmanaged_particle, int swarmSize, int dim)
    {
        double[] swarm = new double[swarmSize * dim];

        Marshal.Copy(unmanaged_particle, swarm, 0, swarm.Length);

        //try
        //{
        //    Marshal.Copy(unmanaged_particle, particle, 0, particle.Length);
        //}
        //finally
        //{
        //    Marshal.FreeHGlobal(unmanaged_particle);
        //}

        double[] result = new double[swarmSize];
        for (int i = 0; i < swarmSize; i++)
        {
            double sum = 0, product = 1;
            for (int j = 0; j < dim; j++)
            {
                sum += Math.Pow(swarm[i * dim + j], 2);
                product *= Math.Cos(swarm[i * dim + j] / Math.Sqrt(i + 1));

                result[i] += (1 + (sum / 4000) - product);
            }

        }
        return MarshalJuggedToC(result);
    }
    // Update is called once per frame

}
