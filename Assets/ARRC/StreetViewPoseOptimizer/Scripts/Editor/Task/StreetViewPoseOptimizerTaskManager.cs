﻿using ARRC.Framework;
using ARRC.DigitalTwinGenerator;
using ARRC.StreetViewPoseOptimizer;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ARRC.StreetViewPoseOptimizer
{
    public class StreetViewPoseOptimizerTaskManager : TaskManager
    {
        StreetViewPoseOptimizer poseOptimizer;
        public void Init(List<GameObject> steetviewList, int equirectangularWidth, int equirectangularHeight, int cubemapWidth,
            float cameraNearPlane, float cameraFarPlane,
            int swarmSize, int iteration, double threshold, bool halt_iteration, bool halt_threshold, HypothesisType hypothesisType,
            bool showResultWindow, string path_resultFolder, EditorWindow wnd)
        {
            poseOptimizer = new StreetViewPoseOptimizer(equirectangularWidth, equirectangularHeight, cubemapWidth, hypothesisType);

            AddTask(new CoroutineEditorTask("Optimizing registration of the images", wnd, poseOptimizer, poseOptimizer.Optimize(
                steetviewList, equirectangularWidth, equirectangularHeight, cameraNearPlane, cameraFarPlane
                , swarmSize, iteration, threshold, halt_iteration, halt_threshold, showResultWindow, path_resultFolder)));
        }

        static StreetViewPoseOptimizerTaskManager instance;
        public static StreetViewPoseOptimizerTaskManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new StreetViewPoseOptimizerTaskManager();

                return instance;
            }
        }

    }

}
